'use strict'

const Joi = require('joi')

module.exports = Joi.object({
  name   : Joi.string().alphanum(),
  weapon : Joi.string().alphanum(),
  points : Joi.number(),
})

