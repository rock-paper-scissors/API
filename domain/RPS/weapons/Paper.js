'use strict'

const Weapon = require('../Weapon')

module.exports = class Paper extends Weapon {

  beats(otherWeapon) {
    return otherWeapon.constructor.name === 'Rock'
  }

}
