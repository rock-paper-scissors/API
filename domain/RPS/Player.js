'use strict'

module.exports = class RPSPlayer {

  constructor(name, weapon) {
    this.name   = name
    this.weapon = weapon
    this.points = 0
  }

}

