'use strict'

const Boom = require('boom')

module.exports = weapon => {
  let err = new Error(`The following weapon doesn't exist: ${weapon}`)
  return Boom.boomify(err, {
    statusCode: 422
  })
}

