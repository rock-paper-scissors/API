'use strict'

const Joi = require('joi')

module.exports = Joi.object({
  "name"             : Joi.string().alphanum(),
  "weapon"           : Joi.string().alphanum().valid(['Rock', 'Paper', 'Scissors']).required(),
  "computer_players" : Joi.number().min(1).max(100),
})

