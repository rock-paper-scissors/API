'use strict'

const RPSGame       = require('^domain/RPS/Game')
const PlayerFactory = require('^domain/RPS/factories/PlayerFactory')

function create(request, h) {

  const numPlayers = parseInt(request.query.players) || 2

  const players = PlayerFactory.CreateComputerPlayers(numPlayers)

  const game = new RPSGame(players)

  const { outcome, survivors, winner } = game.playMatch()

  return h
    .response(JSON.stringify({
      "outcome"   : outcome,
      "winner"    : winner,
      "survivors" : survivors,
      "players"   : players
    }))
    .code(201)
}

module.exports = {
  create
}

