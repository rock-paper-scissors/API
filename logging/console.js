'use strict'

const good = require('good')

module.exports = {
  plugin  : good,
  options : {
    reporters : {
      console : [
        {
          module : 'good-squeeze',
          name   : 'Squeeze',
          args   : [
            {
              log      : '*',
              request  : '*',
              response : '*',
            }
          ]
        },
        {
          module : 'good-console',
          args   : [
            {
              format : '',
            }
          ]
        },
        'stdout'
      ]
    }
  }
}

