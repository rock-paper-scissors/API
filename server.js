'use strict'

const Hapi   = require('hapi')
const rooty  = require('rooty')
const dotenv = require('dotenv-safe')

const PreResponseHook = require('./server-lifecycle-hooks/PreResponse')
const logger          = require('./logging/console')

rooty()
dotenv.config()

const server = Hapi.server({
  host : process.env.HOST,
  port : process.env.PORT,
})

server.route(require('./api/games/routes'))

server.ext('onPreResponse', PreResponseHook)

async function start() {
  try {
    await server.register(logger)
    await server.start()
  }
  catch (err) {
    console.error(err)
    process.exit(1)
  }
  console.log(`Server running at: ${server.info.uri}`)
}

if (!module.parent) {
  start()
}

module.exports = server

