# Rock, Paper, Scissors API

1) Install the dependencies

```
npm i
```

2) Test it

```
npm test
```

3) Run it

```
npm start
```

